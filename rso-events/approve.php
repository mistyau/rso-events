<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/Event.php'; ?>

<?php
$event = new Event;

if (!isset($_SESSION['userid']) || !isset($_SESSION['sa_id']))
{
    redirect('index.php', 'Sign in to access this page', 'error');
}
else
{
    $sa_id = $_SESSION['sa_id'];
}

if (isset($_POST['approve_id']))
{
    $aprv_id = $_POST['approve_id'];

    if ($event->approve($aprv_id))
    {
        redirect('dashboard.php', 'Event successfully approved', 'success');
    }
    else
    {
        redirect('dashboard.php', 'Something went wrong', 'error');
    }
}

$template = new Template('templates/approve-event.php');

$event_id = isset($_GET['id']) ? $_GET['id'] : null;

$template->event = $event->getEvent($event_id);
echo $template;