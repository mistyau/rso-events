<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/Event.php'; ?>
<?php require_once 'lib/University.php'; ?>

<?php
$event = new Event;
$university = new University;

$template = new Template('templates/events-view.php');

if (!isset($_SESSION['userid']))
{
    redirect('index.php', 'Sign in to access this page!', 'error');
}

$userid = $_SESSION['userid'];
$universityid = $_SESSION['school'];

$school = isset($_GET['school']) ? $_GET['school'] : null;

if ($school)
{
    $template->publicEvents = $event->getPublicEventsByUniversity($school);
    $template->title = 'Events at ' . $university->getUniversity($school)['name'];
    if ($school == $universityid)
    {
        $template->privateEvents = $event->getEventsByUniversity($universityid);
    }
    else
    {
        $template->privateEvents = null;
    }
    $template->events = null;
}
else
{
    $template->title = "Your Events";
    $template->events = $event->getRSOEvents($userid);
    $template->publicEvents = $event->getPublicEvents();
    $template->privateEvents = $event->getEventsByUniversity($universityid);
}

$template->universities = $university->getAllUniversities();

echo $template;