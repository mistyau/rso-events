<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/RSO.php'; ?>

<?php
$rso = new RSO;

$rso_id = isset($_GET['id']) ? $_GET['id'] : null;

if (!isset($_SESSION['userid']))
{
    redirect('index.php', 'Must be logged in', 'error');
}

if (!isset($_SESSION['admin_id']))
{
    redirect('index.php', 'You must be the admin of this RSO to host an event!', 'error');
}

if (isset($_POST['rso-event-submit']))
{
    // Create data array
    $data = array();
    $data['name'] = $_POST['re_name'];
    $stime = strtotime($_POST['stime']);
    $new_stime = date('H:i', $stime);
    $data['stime'] = $new_stime;
    $etime = strtotime($_POST['etime']);
    $new_etime = date('H:i', $etime);
    $data['etime'] = $new_etime;
    $date = strtotime($_POST['re_date']);
    $new_date = date('Y-m-d', $date);
    $data['date'] = $new_date;
    $data['category'] = $_POST['re_category'];
    $data['description'] = $_POST['re_description'];
    $data['address'] = $_POST['re_address'];
    $address = $data['address'];
    $prepAddr = str_replace(' ', '+', $address);
    $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' .$prepAddr.'&key=AIzaSyBCkjT_-oWgTQ17_4Atndqwzds6p2rfsAo');
    $output = json_decode($geocode);
    $latitude = $output->results[0]->geometry->location->lat;
    $longitude = $output->results[0]->geometry->location->lng;
    $data['latitude'] = $latitude;
    $data['longitude'] = $longitude;
    $data['email'] = $_POST['re_contact_email'];
    $data['phone'] = $_POST['re_contact_phone'];

    if ($rso->createEvent($rso_id, $data))
    {
        redirect('dashboard.php', 'Your event has been listed', 'success');
    }
    else
    {
        redirect('dashboard.php', 'Something went wrong', 'error');
    }
}

$template = new Template('templates/rso-event-create.php');

$template->rso = $rso->getRSO($rso_id);
$template->categories = $rso->getCategories();

echo $template;
