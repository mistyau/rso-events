<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/RSO.php'; ?>

<?php

$rso = new RSO;

if (!isset($_SESSION['userid']))
{
    redirect('index.php', 'Login to create RSOs!', 'error');
}

if (isset($_POST['rso-submit']))
{
    // Create data array
    $data = array();
    $data['name'] = $_POST['rso_name'];
    $data['description'] = $_POST['rso_description'];
    $data['admin_id'] = $_SESSION['admin_id'];

    if ($rso->create($data))
    {
        redirect('create-rso.php', 'Your RSO has been created', 'success');
    }
    else
    {
        redirect('create-rso.php', 'Something went wrong', 'error');
    }
}

$template = new Template('templates/rso-create.php');

echo $template;
