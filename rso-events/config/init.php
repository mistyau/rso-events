<?php
// Start session
session_start();

// Config File
require_once 'config.php';

// Include Helpers
require_once 'helpers/system_helper.php';

// Autoloader
function __spl_autoload_register($class_name)
{
    require_once 'lib/'. $class_name . '.php';
}