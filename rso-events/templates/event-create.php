<?php include 'inc/header.php'; ?>
        <h2 class="page-header">Create Public/Private Events</h2>
        <form method="post" action="create-pp.php">
            <div class="form-group">
                <label>Event Name</label>
                <input type="text" class="form-control" name="ename">
            </div>
            <div class="form-group">
                <label>Event Type</label>
                <select class="form-control" name="type">
                    <option value=0>Public</option>
                    <option value=1>Private</option>
                </select>
            </div>
            <div class="form-group">
                <label>Category</label>
                <select class="form-control" name="category">
                    <option value="0">Choose Category</option>
                    <?php foreach($categories as $category): ?>
                        <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description"></textarea>
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" name="address" id="address">
                <br><br>
                <div id="map" style="width: 500px; height: 400px; margin: 0 auto;"></div>
                <script>
                    var map;
                    function initMap() {
                        var myLatLng = {lat: 28.5968774, lng: -81.2033083};
                        var geocoder = new google.maps.Geocoder;
                        
                        map = new google.maps.Map(document.getElementById('map'), {
                            center: myLatLng,
                            zoom: 8 
                        });

                        map.addListener('click', function(e) {
                            getAddress(e.latLng);
                            placeMarkerAndPanTo(e.latLng, map);
                        });

                        function placeMarkerAndPanTo(latLng, map) {
                            var marker = new google.maps.Marker({
                                position: latLng,
                                map: map
                            });
                            map.panTo(latLng);
                        }

                        function getAddress(latLng) {
                            geocoder.geocode({ 'latLng': latLng }, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {
                                        document.getElementById("address").value = results[0].formatted_address;
                                    }
                                }
                            });
                        }
                    }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCkjT_-oWgTQ17_4Atndqwzds6p2rfsAo&callback=initMap" async defer></script>
            </div>
            <div class="form-group">
                <label>Start time</label>
                <input type="time" class="form-control" name="stime">
            </div>
            <div class="form-group">
                <label>End time</label>
                <input type="time" class="form-control" name="etime">
            </div>
            <div class="form-group">
                <label>Date</label>
                <input type="date" class="form-control" name="date">
            </div>
            <div class="form-group">
                <label>Contact Phone</label>
                <input type="text" class="form-control" name="phone">
            </div>
            <div class="form-group">
                <label>Contact Email</label>
                <input type="text" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label>Get approval from</label>
                <select class="form-control" name="approval">
                    <?php foreach($superAdmins as $superAdmin): ?>
                        <option value="<?php echo $superAdmin['superAdmin_ID']; ?>"><?php echo $superAdmin['uname']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="submit" class="btn btn-success btn-block" value="Submit" name="event-submit">
        </form>

<?php include 'inc/footer.php'; ?>