<?php include 'inc/header.php'; ?>
        <h2 class="page-header">Create Account</h2>
        <form method="post" action="signup.php">
            <!--
            <div class="form-group">
                <label>Account type</label>
                <select class="form-control" name="user_type">
                    <option value=1>Student</option>
                    <option value=2>Admin</option>
                    <option value=3>Super Admin</option>
                </select>
            </div>
            -->

            <div class="form-group">
                <label>School</label>
                <select class="form-control" name="school">
                <option value="0">Choose School</option>
                    <?php foreach($universities as $university): ?>
                        <option value="<?php echo $university['universityid']; ?>"><?php echo $university['name']; ?></option>
<                   <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="username">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="pwd">
            </div>
            <div class="form-group">
                <label>Repeat password</label>
                <input type="password" class="form-control" name="pwd-repeat">
            </div>
            <input type="submit" class="btn btn-success btn-lg btn-block" value="Submit" name="signup-submit">
        </form>
<?php include 'inc/footer.php'; ?>