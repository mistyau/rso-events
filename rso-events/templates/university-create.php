<?php include 'inc/header.php'; ?>
        <h2 class="page-header">Create University Profile</h2>
        <form method="post" action="create.php">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="uni_name">
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" name="uni_address">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="uni_description"></textarea>
            </div>
            <input type="submit" class="btn btn-success btn-block" value="Submit" name="university-submit">
        </form>
<?php include 'inc/footer.php'; ?>