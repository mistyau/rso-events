<?php include 'inc/header.php'; ?>
      <div class="jumbotron">
        <h1 class="display-3">Search for Events</h1>
        <form method="GET" action="view-events.php">
          <select name="school" class="form-control">
            <option value="0">Choose School</option>
            <?php foreach($universities as $university): ?>
                <option value="<?php echo $university['universityid']; ?>"><?php echo $university['name']; ?></option>
<           <?php endforeach; ?>
          </select>
          <br>
          <input type="submit" class="btn btn-lg btn-success" value="SEARCH">
        </form>
      </div>

<h3><?php echo $title; ?></h3>
      <?php if ($events instanceof Traversable) {
      foreach($events as $event): ?>
      <div class="row marketing">
        <div class="col-md-10">
          <h4><?php echo $event['name']; ?>
          <small class="text-muted">RSO</small></h4>
          <p><?php echo $event['description']; ?></p>
        </div>
        <div class="col-md-2">
            <a class="btn btn-outline-secondary" href="event.php?id=<?php echo $event['event_ID']; ?>">View</a>
        </div>
      </div>
      <?php endforeach; }?>
      <?php if ($publicEvents instanceof Traversable) {
      foreach($publicEvents as $event): ?>
      <div class="row marketing">
        <div class="col-md-10">
          <h4><?php echo $event['name']; ?>
          <small class="text-muted">Public</small></h4>
          <p><?php echo $event['description']; ?></p>
        </div>
        <div class="col-md-2">
            <a class="btn btn-outline-secondary" href="event.php?id=<?php echo $event['event_ID']; ?>">View</a>
        </div>
      </div>
      <?php endforeach; } ?>
      <?php if ($privateEvents instanceof Traversable) {
      foreach($privateEvents as $event): ?>
      <div class="row marketing">
        <div class="col-md-10">
          <h4><?php echo $event['name']; ?>
          <small class="text-muted">Private</small></h4>
          <p><?php echo $event['description']; ?></p>
        </div>
        <div class="col-md-2">
            <a class="btn btn-outline-secondary" href="event.php?id=<?php echo $event['event_ID']; ?>">View</a>
        </div>
      </div>
      <?php endforeach; } ?>
      
<?php include 'inc/footer.php'; ?>