<?php include 'inc/header.php'; ?>
        <h2 class="page-header">Create RSO</h2>
        <form method="post" action="create-rso-student.php">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="rso_name">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="rso_description"></textarea>
            </div>
            <div class="form-group">
                <label>Student 1</label>
                <input type="text" class="form-control" name="student1">
            </div>
            <div class="form-group">
                <label>Student 2</label>
                <input type="text" class="form-control" name="student2">
            </div>
            <div class="form-group">
                <label>Student 3</label>
                <input type="text" class="form-control" name="student3">
            </div>
            <div class="form-group">
                <label>Student 4</label>
                <input type="text" class="form-control" name="student4">
            </div>
            <div class="form-group">
                <label>Student 5</label>
                <input type="text" class="form-control" name="student5">
            </div>
            <input type="submit" class="btn btn-success btn-block" value="Submit" name="rso-submit">
        </form>
<?php include 'inc/footer.php'; ?>