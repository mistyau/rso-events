<?php include 'inc/header.php'; ?>
    <h2 class="page-header"><?php echo $event['name']; ?></h2>
    <small>Posted by: <?php echo $event['admin']; ?>@<?php echo $event['school']; ?></small>
    <hr>
    <p class="lead"><?php echo $event['description']; ?></p>
    <ul class="list-group">
        <?php if ($event['rname'])
        {
            echo '<li class="list-group-item"><strong>RSO: </strong>'.$event['rname'].'</li>';
        }
        ?>
        <li class="list-group-item"><strong>Category:</strong> <?php echo $event['cname']; ?></li>
        <li class="list-group-item"><strong>Date:</strong> <?php echo $event['date']; ?></li>
        <li class="list-group-item"><strong>Start time:</strong> <?php echo $event['stime']; ?></li>
        <li class="list-group-item"><strong>End time:</strong> <?php echo $event['etime']; ?></li>
        <li class="list-group-item"><strong>Location:</strong> <?php echo $event['address']; ?></li>
        <li class="list-group-item"><strong>Contact Email:</strong> <?php echo $event['email']; ?></li>
        <li class="list-group-item"><strong>Contact Phone:</strong> <?php echo $event['phone']; ?></li>
    </ul>

    <br><br>
    <br><br>
    <a href="dashboard.php">Go Back</a>
    <br><br>
   
<!-- Post Comment -->
<form method=post action="event.php?id=<?php echo $event['event_ID']; ?>">
    <div class="well">
        <div class="form-group">
            <label>Leave a comment:</label>
            <textarea class="form-control" name="comment" placeholder="Sounds cool! Count me in :)"></textarea>
        </div>
        <!-- Ratings -->
        <div class="rate">
          <input type="radio" id="star5" name="rate" value="5" />
          <label for="star5" title="Excellent">5 stars</label>
          <input type="radio" id="star4" name="rate" value="4" />
          <label for="star4" title="Good">4 stars</label>
          <input type="radio" id="star3" name="rate" value="3" />
          <label for="star3" title="Average">3 stars</label>
          <input type="radio" id="star2" name="rate" value="2" />
          <label for="star2" title="Poor">2 stars</label>
          <input type="radio" id="star1" name="rate" value="1" />
          <label for="star1" title="Terrible">1 star</label>
        </div>
        <input type="submit" class="btn btn-success" value="Post" name="comment-submit">
    </div>
</form>
<br><br>

<!-- Display Comments -->
<h5 class="page-header">Comments</h5>
<hr>
<div class="well">
    <ul class="media-list">
        <?php foreach($comments as $comment): ?>
        <li class="media">
            <div class="media-body">
                <strong class="text-success">@<?php echo $comment['uname']; ?></strong>
                <span class="text-muted pull-right">
                    <small class="text-muted"><?php echo $comment['timestamp']; ?></small>
                </span>
                <p>
                    <?php echo $comment['text']; ?>
                </p>
                <?php if ($comment['userid'] == $_SESSION['userid']) {
                        echo '<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#edit">Edit</button>';
                        echo'
                        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#delete">Delete</button>';
                    }
                ?>
            </div>
        </li>
        <br><br>
        <?php endforeach; ?>
    </ul>
</div>

<!-- Modal -->
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form method="post" action="event.php?id=<?php echo $event['event_ID']; ?>">
        <div class="modal-body">
            <textarea class="form-control" name="new_comment"></textarea>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-success" value="Submit" name="edit-submit">
        </div>
      </form>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="delete" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form style="display:inline;" method="post" action="event.php">
            <input type="hidden" name="del_id" value="<?php echo $event['event_ID']; ?>">
            <input type="submit" class="btn btn-danger btn-block" value="Confirm delete?">
        </form>
      </div>
    </div>

  </div>
</div>

<?php include 'inc/footer.php'; ?>