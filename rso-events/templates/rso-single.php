<?php include 'inc/header.php'; ?>
    <h2 class="page-header"><?php echo $rso['name']; ?></h2>
    <small>Admin: <?php echo $rso['admin']; ?>@<?php echo $rso['school']; ?>, Status: <?php echo $rso['status']; ?></small>
    <hr>
    <p class="lead"><?php echo $rso['description']; ?></p>
    <br><br>
    <a href="dashboard.php">Go Back</a>
    <br><br>
   
    <div class="well">
        <?php if (isset($_SESSION['admin_id']))
        {
            if ($_SESSION['admin_id'] == $rso['adminid'])
            {
                echo '<a href="host.php?id='.$rso['rso_ID'].'" class="btn btn-outline-primary">Host an Event</a>';
            }
        }
        ?>
        <form style="display:inline;" method="post" action="rso.php">
            <?php  if (!$joined)
                {
                    echo '<input type="hidden" name="join_id" value="'.$rso['rso_ID'].'">';
                    echo '<input type="submit" class="btn btn-primary" value="Join">';
                }
                else
                {
                    echo '<input type="hidden" name="leave_id" value="'.$rso['rso_ID'].'">';
                    echo '<input type="submit" class="btn btn-outline-danger" value="Leave">';
                }
            ?>
        </form>
    </div>

<?php include 'inc/footer.php'; ?>