<?php include 'inc/header.php'; ?>
        <h2 class="page-header">Create RSO</h2>
        <form method="post" action="create-rso.php">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="rso_name">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="rso_description"></textarea>
            </div>
            <input type="submit" class="btn btn-success btn-block" value="Submit" name="rso-submit">
        </form>
<?php include 'inc/footer.php'; ?>