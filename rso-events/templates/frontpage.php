<?php include 'inc/header.php'; ?>
        <h2 class="page-header">Sign in</h2>
        <form method="post" action="login.php">
            <div class="form-group">
                <label>Username/E-mail</label>
                <input type="text" class="form-control" name="mailuid">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="pwd">
            </div>
            <input type="submit" class="btn btn-success" value="Sign in" name="login-submit">
            <a href="signup.php">Sign up</a>
        </form>
<?php include 'inc/footer.php'; ?>