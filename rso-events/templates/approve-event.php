<?php include 'inc/header.php'; ?>
    <h2 class="page-header"><?php echo $event['name']; ?></h2>
    <small>Admin:<?php echo $event['admin']; ?></small>
    <hr>
    <p class="lead"><?php echo $event['description']; ?></p>
    <ul class="list-group">
        <li class="list-group-item"><strong>Category:</strong> <?php echo $event['cname']; ?></li>
        <li class="list-group-item"><strong>Date:</strong> <?php echo $event['date']; ?></li>
        <li class="list-group-item"><strong>Start time:</strong> <?php echo $event['stime']; ?></li>
        <li class="list-group-item"><strong>End time:</strong> <?php echo $event['etime']; ?></li>
        <li class="list-group-item"><strong>Location:</strong> <?php echo $event['address']; ?></li>
        <li class="list-group-item"><strong>Contact Email:</strong> <?php echo $event['email']; ?></li>
        <li class="list-group-item"><strong>Contact Phone:</strong> <?php echo $event['phone']; ?></li>
    </ul>

    <br><br>
    <a href="dashboard.php">Go Back</a>
    <br><br>

    <div class="well">
        <form style="display:inline;" method="post" action="approve.php">
            <input type="hidden" name="approve_id" value="<?php echo $event['event_ID']; ?>">
            <input type="submit" class="btn btn-success" value="Approve">
        </form>
    </div>

<?php include 'inc/footer.php'; ?>