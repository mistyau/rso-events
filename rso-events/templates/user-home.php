<?php include 'inc/header.php'; ?>
    <h3><?php echo $title; ?></h3>

    <?php if (isset($_SESSION['sa_id'])) {
      echo '<hr>';
      echo '<h4 class="text-muted">Events waiting for approval</h4>';
      foreach($events as $event):
          echo '<div class="row marketing">
                    <div class="col-md-10">
                    <h4>'.$event['name'].'</h4>
                    <p>'.$event['description'].
                  '</div>
                  <div class="col-md-2">
                    <a class="btn btn-outline-secondary" href=approve.php?id='.$event['event_ID'].'>View</a>
                  </div>
                </div>';
      endforeach;    
      echo '<hr>';
    } ?>

    <h4 class="text-muted">RSOs at
    <?php echo $school?></h4>
      <?php foreach($rsos as $rso): ?>
      <div class="row marketing">
        <div class="col-md-10">
          <h4><?php echo $rso['name']; ?></h4>
          <p><?php echo $rso['description']; ?></p>
        </div>
        <div class="col-md-2">
            <a class="btn btn-outline-secondary" href="rso.php?id=<?php echo $rso['rso_ID']; ?>">View</a>
        </div>
      </div>
      <?php endforeach; ?>

<?php include 'inc/footer.php'; ?>