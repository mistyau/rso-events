<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>College Events</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<div class="container">
      <div class="header clearfix">
        <nav>

          <?php if (isset($_SESSION['userid']))
          {
            echo '<ul class="nav nav-pills float-right">
                    <li class="nav-item">
                      <a class="nav-link" href="view-events.php">View Events</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                  </ul>';
          }
          ?>

          <?php if (isset($_SESSION['admin_id']))
          {
            echo '<ul class="nav nav-pills float-right">
                    <li class="nav-item">
                      <a class="nav-link" href="create-rso.php">Create RSO</a>
                    </li>
                    
                  </ul>';

            echo '<ul class="nav nav-pills float-right">
                    <li class="nav-item">
                      <a class="nav-link" href="create-pp.php">Create Event</a>
                    </li>
                  </ul>';
          }
          ?>

          <?php if (isset($_SESSION['sa_id']))
          {
            echo '<ul class="nav nav-pills float-right">
                  <a class="nav-link" href="create.php">Create University Profile</a>
                  </ul>';
          }
          ?>

          <?php if (!isset($_SESSION['admin_id']) && !isset($_SESSION['sa_id']) && isset($_SESSION['userid']))
          {
            echo '<ul class="nav nav-pills float-right">
              <li class="nav-item">
                <a class="nav-link" href="create-rso-student.php">Request RSO</a>
              </li>
            </ul>';
          }
          ?>
          
        </nav>
        <h3 class="text-muted"><a href=<?php  if (isset($_SESSION['userid']))
                                                  echo "dashboard.php";
                                              else
                                                  echo "index.php"; ?>>
        <?php echo SITE_TITLE?></a></h3>
      </div>
      <?php displayMessage();?>