
    <div id="map" style="width: 500px; height: 400px;"></div>
    <script>
      var map;
      function initMap() {
        var myLatLng = {lat: 28.5968774, lng: -81.2033083};
        var geocoder = new google.maps.Geocoder;          
        map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 8 
        });

        map.addListener('click', function(e) {
          //placeMarkerAndPanTo(e.latLng, map);
          geocodeLatLng(e.latLng);
          //alert(e.latLng);
        });

        function placeMarkerAndPanTo(latLng, map) {
          var marker = new google.maps.Marker({
            position: latLng,
            map: map
          });
          map.panTo(latLng);
        }

        function geocodeLatLng(latLng) {
          geocoder.geocode({ 'latLng': latLng }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                alert(results[0].formatted_address);
              }
            }
          });
        }
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCkjT_-oWgTQ17_4Atndqwzds6p2rfsAo&callback=initMap"
    async defer></script>

    
