<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php

if (isset($_POST['login-submit']))
{
    $mailuid = $_POST['mailuid'];
    $password = $_POST['pwd'];

    if (empty($mailuid) || empty($password)) {
        redirect('login.php', 'Empty field(s)', 'error');
    }
    else {
        $sql = "SELECT * FROM users WHERE username=? OR email=?;";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../index.php?error=sqlerror");
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result)) {
                $pwdCheck = password_verify($password, $row['pwd']);
                if ($pwdCheck == false) {
                    redirect('login.php', 'Wrong password', 'error');
                }
                else if ($pwdCheck == true) {
                    session_start();
                    $_SESSION['userid'] = $row['user_id'];
                    $_SESSION['uname'] = $row['username'];
                    $_SESSION['school'] = $row['school'];

                    redirect('dashboard.php', 'You are logged in', 'success');
                }
                else {
                    redirect('login.php', 'Wrong password', 'error');
                }
            }
            else {
                redirect('login.php', 'User does not exist', 'error');
            }
        }
    }
}

$template = new Template('templates/frontpage.php');

echo $template;
