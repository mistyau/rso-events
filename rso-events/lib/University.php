<?php
    class University {
        private $conn;

        public function __construct()
        {
            // Error reporting
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

           $this->conn = mysqli_connect("localhost", "root", "root", "college_events");

           if (!$this->conn)
           {
               die("Connection failed: ".mysqli_connect_error());
           }
        }

        public function getAllUniversities()
        {
            $sql = "SELECT * FROM universities";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function getUniversity($id)
        {
            $sql = "SELECT * FROM universities WHERE universityid = ?";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);
            $row = mysqli_fetch_assoc($result);

            return $row;
        }

        public function create($data)
        {
            // Foreign key goes first
            $sql = "INSERT INTO location (address, longitude, latitude) VALUES (?, ?, ?)";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "sss", $data['address'], $data['longitude'],
                                   $data['latitude']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            $sql = "INSERT INTO universities (name, description, superAdmin_ID, location_id) VALUES (?, ?, ?, ?)";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "ssss", $data['name'], $data['description'], $data['superAdmin_id'],                                        mysqli_insert_id($this->conn));
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }
    }