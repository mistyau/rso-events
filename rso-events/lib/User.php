<?php
    class User
    {
        private $conn;

        public function __construct()
        {
            // Error reporting
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

            $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if (!$this->conn)
            {
               die("Connection failed: ".mysqli_connect_error());
            }
        }

        public function getAllSuperAdmins()
        {
            $sql = "SELECT super_admins.*, users.username AS uname
                    FROM super_admins
                    INNER JOIN users ON users.user_id = super_admins.superAdmin_ID";

            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

    }