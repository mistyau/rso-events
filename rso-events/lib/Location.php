<?php
    class Location
    {
        private $conn;

        public function __construct()
        {
            // Error reporting
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

            $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if (!$this->conn)
            {
               die("Connection failed: ".mysqli_connect_error());
            }
        }

        public function getLoc($lng, $lat)
        {
            $sql = "SELECT id FROM location WHERE longitude=? AND latitude=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "ss", $lng, $lat);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result));
            {
                return $row;
            }

            return null;
        }

        public function createLocation($data)
        {
            $sql = "INSERT INTO location (address, longitude, latitude) VALUES (?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "sss", $data['address'], $data['lng'], $data['lat']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

    }