<?php
    class RSO
    {
        private $conn;

        public function __construct()
        {
            // Error reporting
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

            $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if (!$this->conn)
            {
               die("Connection failed: ".mysqli_connect_error());
            }
        }

        public function getLoc($address)
        {
            $sql = "SELECT id FROM location WHERE address=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $address);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result));
            {
                return $row;
            }

            return null;
        }

        public function isOverlapping($lid, $date, $etime, $stime)
        {
            $sql = "SELECT COUNT(*) AS count
                    FROM events E
                    WHERE (E.location_id = ?) 
                    AND (DATEDIFF(E.date, ?) = 0) 
                    AND (SUBTIME(?, E.stime) > 0) 
                    AND (SUBTIME(E.etime, ?) > 0)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return true;
            }
            mysqli_stmt_bind_param( $stmt, "ssss", $lid, $date,
                                    $etime, $stime);
            if (!mysqli_stmt_execute($stmt))
            {
                return true;
            }

            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result))
            {
                if ($row['count'] > 0)
                {
                    return true;
                }
            }
            
            return false;
        }

        public function createEvent($rso_id, $data)
        {

            if ($row = $this->getLoc($data['address']))
            {
                $locId = $row['id'];
            }
            else
            {
                $sql = "INSERT INTO location (address, longitude, latitude) VALUES (?, ?, ?)";
                $stmt =  mysqli_stmt_init($this->conn);
                if (!mysqli_stmt_prepare($stmt, $sql))
                {
                    return false;
                }
                mysqli_stmt_bind_param( $stmt, "sss", $data['address'], $data['longitude'],
                                        $data['latitude']);
                if (!mysqli_stmt_execute($stmt))
                {
                    return false;
                }

                $locId = mysqli_insert_id($this->conn);
            }

            if ($this->isOverlapping($locId, $data['date'], $data['etime'], $data['stime']))
            {
                return false;
            }

            $sql = "INSERT INTO events (event_ID, name, category, description, stime, etime, date, location_id, email,                             phone) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            $lid =  mysqli_insert_id($this->conn);
            mysqli_stmt_bind_param($stmt, "sssssssss", $data['name'], $data['category'], $data['description'],                          $data['stime'], $data['etime'], $data['date'], $locId, $data['email'], $data['phone']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }
            $sql = "INSERT INTO rso_events (event_ID, rso_ID) VALUES (?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            $rid = mysqli_insert_id($this->conn);
            mysqli_stmt_bind_param($stmt, "ss", $rid, $rso_id);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function getCategories()
        {
            $sql = "SELECT * FROM categories";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function getCategory($category_id)
        {
            $sql = "SELECT * FROM categories";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $category_id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result));
            {
                return $row;
            }

            return null;
        }

        public function isJoined($uid, $id)
        {
            $sql = "SELECT  * 
                    FROM    joins J
                    WHERE   J.rso_ID=? AND J.user=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "ss", $id, $uid);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if ($resultCheck > 0)
            {
                return true;
            }

            return false;
        }

        public function join($uid, $id)
        {
            $sql = "INSERT INTO joins (user, rso_ID) VALUES (?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "ss", $uid, $id);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function leave($uid, $id)
        {
            $sql = "DELETE
                    FROM joins 
                    WHERE rso_ID=? AND user=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "ss", $id, $uid);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function getRSO($id)
        {
            $sql = "SELECT * FROM rso WHERE rso_ID=?";

            $sql = "SELECT rso.*, universities.name AS school, users.username AS admin
                    FROM rso
                    INNER JOIN admins ON rso.adminid = admins.admin_ID
                    INNER JOIN users ON admins.admin_ID = users.user_id
                    INNER JOIN universities ON users.school = universities.universityid
                    WHERE rso.rso_ID=?";

            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result));
            {
                return $row;
            }

            return null;
        }

        public function getAllRSOs()
        {
            $sql = "SELECT * FROM rso";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function getRSOsByUniversity($university_id)
        {
            $sql = "SELECT  * 
                    FROM    rso R 
                    WHERE   R.adminid IN (  SELECT  A.admin_ID 
                                            FROM    admins A 
                                            WHERE   A.admin_ID IN (SELECT U.user_id 
                                                                FROM users U
                                                                WHERE U.school=?))";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "s", $university_id);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function create($data)
        {
            $sql = "INSERT INTO rso (name, adminid, description) VALUES (?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "sss", $data['name'], $data['admin_id'], $data['description']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }
    }