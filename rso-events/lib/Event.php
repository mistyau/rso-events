<?php
    class Event
    {
        private $conn;

        public function __construct()
        {
            // Error reporting
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

            $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if (!$this->conn)
            {
               die("Connection failed: ".mysqli_connect_error());
            }
        }

        public function createLocation($data)
        {
            $sql = "INSERT INTO location (address, longitude, latitude) VALUES (?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "sss", $data['address'], $data['lng'], $data['lat']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function getLoc($address)
        {
            $sql = "SELECT id FROM location WHERE address=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $address);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result));
            {
                return $row;
            }

            return null;
        }

        public function getCategories()
        {
            $sql = "SELECT * FROM categories";
            $stmt = mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function approve($eid)
        {
            $sql = "UPDATE  private_events
                    SET     isApproved=1
                    WHERE   event_ID=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "s", $eid);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            $sql = "UPDATE  public_events
            SET     isApproved=1
            WHERE   event_ID=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "s", $eid);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function getEvent($id)
        {
            $sql = "SELECT  events.*, location.address AS address, categories.name AS cname,
                            rso.name AS rname, users.username AS admin, universities.name AS school
                    FROM    events
                    INNER JOIN  location ON events.location_id = location.id
                    INNER JOIN  categories ON events.category = categories.id
                    LEFT JOIN   rso_events ON events.event_ID = rso_events.event_ID
                    LEFT JOIN   rso ON rso_events.rso_ID = rso.rso_ID
                    LEFT JOIN   private_events PE ON events.event_ID = PE.event_ID
                    LEFT JOIN   public_events PU ON events.event_ID = PU.event_ID
                    INNER JOIN  admins R ON rso.adminid = R.admin_ID OR PE.admin_ID = R.admin_ID OR PU.admin_ID =             R.admin_ID
                    INNER JOIN  users ON R.admin_ID = users.user_id
                    INNER JOIN  universities ON users.school = universities.universityid
                    WHERE   events.event_ID=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result));
            {
                return $row;
            }

            return null;
        }

        public function deleteComment($uid, $eid)
        {
            $sql = "DELETE 
                    FROM user_comments 
                    WHERE userid=? AND eventid=?";
             $stmt =  mysqli_stmt_init($this->conn);
             if (!mysqli_stmt_prepare($stmt, $sql))
             {
                 return false;
             }
             mysqli_stmt_bind_param($stmt, "ss", $uid, $eid);
             if (!mysqli_stmt_execute($stmt))
             {
                 return false;
             }

             return true;
        }

        public function editComment($uid, $data)
        {
            $sql = "UPDATE  user_comments
                    SET     text=?, timestamp=?
                    WHERE   userid=? AND eventid=?";
             $stmt =  mysqli_stmt_init($this->conn);
             if (!mysqli_stmt_prepare($stmt, $sql))
             {
                 return false;
             }
             mysqli_stmt_bind_param($stmt, "ssss", $data['text'], $data['time'], $uid, $data['eid']);
             if (!mysqli_stmt_execute($stmt))
             {
                 return false;
             }

             return true;
        }

        // Get RSO events
        public function getRSOEvents($user_id)
        {
            $sql = "SELECT  * 
                    FROM    events E 
                    WHERE   E.event_ID IN ( SELECT  R.event_ID
                                            FROM    rso_events R
                                            WHERE   R.rso_ID IN (SELECT J.rso_ID
                                                                 FROM   joins J
                                                                 WHERE  J.user=?))";

            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $user_id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        // Get private events
        public function getEventsByUniversity($university_id)
        {
            $sql = "SELECT  * 
                    FROM    events E
                    WHERE   E.event_ID IN ( SELECT  P.event_ID
                                            FROM    private_events P
                                            WHERE   P.isApproved=true AND P.admin_ID IN (SELECT U.user_id 
                                                                                         FROM users U
                                                                                         WHERE U.school=?))";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $university_id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);
            return $result;
        }

        public function getPublicEventsByUniversity($id)
        {
            $sql = "SELECT  * 
            FROM    events E
            WHERE   E.event_ID IN ( SELECT  P.event_ID
                                    FROM    public_events P
                                    WHERE   P.isApproved=true AND P.admin_ID IN (SELECT  A.admin_ID
                                                                                FROM    admins A
                                                                                WHERE   A.admin_ID IN (SELECT U.user_id 
                                                                                 FROM users U
                                                                                 WHERE U.school=?)))";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $university_id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);
            return $result;
        }

        // Get public events
        public function getPublicEvents()
        {
            $sql = "SELECT  * 
                    FROM    events 
                    WHERE   event_ID IN (   SELECT  P.event_ID
                                            FROM    public_events P
                                            WHERE   P.isApproved=true)";

            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);
            return $result;
        }

        public function getAllEvents()
        {
            $sql = "SELECT * FROM events";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function comment($userID, $data)
        {
            $sql = "INSERT INTO user_comments (userid, eventid, timestamp, text, rating) VALUES (?, ?, ?, ?, ?)";

            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param($stmt, "sssss", $userID, $data['eventid'], $data['timestamp'], $data['text'], $data['rating']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function getAllComments($id)
        {
            $sql = "SELECT user_comments.*, users.username AS uname 
                    FROM user_comments
                    INNER JOIN users ON user_comments.userid = users.user_id
                    WHERE user_comments.eventid=?";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "s", $id);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }

            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

        public function isOverlapping($lid, $date, $etime, $stime)
        {
            $sql = "SELECT COUNT(*) AS count
                    FROM events E
                    WHERE (E.location_id = ?) 
                    AND (DATEDIFF(E.date, ?) = 0)
                    AND (SUBTIME(?, E.stime) > 0) 
                    AND (SUBTIME(E.etime, ?) > 0)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return true;
            }
            mysqli_stmt_bind_param( $stmt, "ssss", $lid, $date,
                                    $etime, $stime);
            if (!mysqli_stmt_execute($stmt))
            {
                return true;
            }

            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result))
            {
                if ($row['count'] > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public function create($data)
        {
            
            if ($row = $this->getLoc($data['address']))
            {
                $locId = $row['id'];
            }
            else
            {
                $sql = "INSERT INTO location (address, longitude, latitude) VALUES (?, ?, ?)";
                $stmt =  mysqli_stmt_init($this->conn);
                if (!mysqli_stmt_prepare($stmt, $sql))
                {
                    return false;
                }
                mysqli_stmt_bind_param( $stmt, "sss", $data['address'], $data['longitude'],
                                        $data['latitude']);
                if (!mysqli_stmt_execute($stmt))
                {
                    return false;
                }

                $locId = mysqli_insert_id($this->conn);
            }

            if ($this->isOverlapping($locId, $data['date'], $data['etime'], $data['stime']))
            {
                return false;
            }

            $sql = "INSERT INTO events (name, category, description, stime, etime, date, location_id,
                    email, phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param( $stmt, "sssssssss", $data['name'], $data['category'],
                                    $data['description'], $data['stime'], $data['etime'], $data['date'],
                                    $locId, $data['email'], $data['phone']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function createPublicEvent($adminId, $data)
        {
            if (!($this->create($data)))
            {
                return false;
            }
            $eid = mysqli_insert_id($this->conn);

            $sql = "INSERT INTO public_events (event_ID, superAdmin_ID, admin_ID) VALUES (?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param( $stmt, "sss", $eid, $data['superAdminID'], $adminId);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function createPrivateEvent($adminID, $data)
        {
            if (!($this->create($data)))
            {
                return false;
            }
            $eid = mysqli_insert_id($this->conn);

            $sql = "INSERT INTO private_events (event_ID, admin_ID, superAdmin_ID) VALUES (?, ?, ?)";
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return false;
            }
            mysqli_stmt_bind_param( $stmt, "sss", $eid, $adminID, $data['superAdminID']);
            if (!mysqli_stmt_execute($stmt))
            {
                return false;
            }

            return true;
        }

        public function getUnapprovedEvents($sid)
        {
            $sql = "SELECT  *
                    FROM    events
                    WHERE   event_ID IN (   SELECT  P.event_ID
                                            FROM    public_events P
                                            WHERE   P.superAdmin_ID=? AND P.isApproved=false)
                    UNION
                    SELECT  *
                    FROM    events
                    WHERE   event_ID IN (   SELECT  P.event_ID
                                            FROM    private_events P
                                            WHERE   P.superAdmin_ID=? AND P.isApproved=false)";
            
            $stmt =  mysqli_stmt_init($this->conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                return null;
            }
            mysqli_stmt_bind_param($stmt, "ss", $sid, $sid);
            if (!mysqli_stmt_execute($stmt))
            {
                return null;
            }
            $result = mysqli_stmt_get_result($stmt);

            return $result;
        }

    }