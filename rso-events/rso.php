<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/RSO.php'; ?>

<?php
$rso = new RSO;

if (!isset($_SESSION['userid']))
{
    redirect('index.php', 'Must sign in to access that page', 'error');
}

$uid = $_SESSION['userid'];

if (isset($_POST['join_id']) && isset($_SESSION['userid']))
{
    $join_id = $_POST['join_id'];
    if ($rso->join($uid, $join_id))
    {
        redirect('dashboard.php', 'Joined new RSO', 'success');
    }
    else
    {
        redirect('dashboard.php', 'Could not join RSO', 'error');
    }
}

if (isset($_POST['leave_id']) && isset($_SESSION['userid']))
{
    $leave_id = $_POST['leave_id'];
    if ($rso->leave($uid, $leave_id))
    {
        redirect('dashboard.php', 'Left RSO', 'success');
    }
    else
    {
        redirect('dashboard.php', 'Could not leave RSO', 'error');
    }
}

$template = new Template('templates/rso-single.php');

$rso_id = isset($_GET['id']) ? $_GET['id'] : null;

$template->rso = $rso->getRSO($rso_id);
$template->joined = $rso->isJoined($uid, $rso_id);

echo $template;