<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/Event.php'; ?>
<?php require_once 'lib/User.php'; ?>

<?php
$event = new Event;
$user = new User;

if (!isset($_SESSION['userid']) || !isset($_SESSION['admin_id']))
{
    redirect('index.php', 'Must be logged in as admin', 'error');
}

$adminID = $_SESSION['admin_id'];

if (isset($_POST['event-submit']))
{
    // Create data array
    $data = array();
    $data['name'] = $_POST['ename'];
    $time = strtotime($_POST['stime']);
    $new_time = date('H:i', $time);
    $data['stime'] = $new_time;
    $time = strtotime($_POST['etime']);
    $new_time = date('H:i', $time);
    $data['etime'] = $new_time;
    $date = strtotime($_POST['date']);
    $new_date = date('Y-m-d', $date);
    $data['date'] = $new_date;
    $data['category'] = $_POST['category'];
    $data['description'] = $_POST['description'];
    $data['address'] = $_POST['address'];
    $address = $data['address'];
    $prepAddr = str_replace(' ', '+', $address);
    $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' .$prepAddr.'&key=AIzaSyBCkjT_-oWgTQ17_4Atndqwzds6p2rfsAo');
    $output = json_decode($geocode);
    $latitude = $output->results[0]->geometry->location->lat;
    $longitude = $output->results[0]->geometry->location->lng;
    $data['latitude'] = $latitude;
    $data['longitude'] = $longitude;
    $data['email'] = $_POST['email'];
    $data['phone'] = $_POST['phone'];
    $data['superAdminID'] = $_POST['approval'];
    
    if ($_POST['type'] == 0)
    {
        if ($event->createPublicEvent($adminID, $data))
        {
            redirect('dashboard.php', 'Public event successfully requested', 'success');
        }
        else
        {
            redirect('create-pp.php', 'Something went wrong', 'error');
        }
    }
    else if ($_POST['type'] == 1)
    {
        if ($event->createPrivateEvent($adminID, $data))
        {
            redirect('dashboard.php', 'Private event successfully requested', 'success');
        }
        else
        {
            redirect('create-pp.php', 'Something went wrong', 'error');
        }
    }
    else
    {
        redirect('create-pp.php', 'Something went wrong', 'error');
    }
}

$template = new Template('templates/event-create.php');

$template->categories = $event->getCategories();
$template->superAdmins = $user->getAllSuperAdmins();

echo $template;
