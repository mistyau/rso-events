<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/RSO.php'; ?>
<?php require_once 'lib/Event.php'; ?>

<?php
$rso = new RSO;
$event = new Event;
$university = new University;

$template = new Template('templates/user-home.php');

if (isset($_SESSION['userid']))
{
    $userid = $_SESSION['userid'];

    $sql = "SELECT * FROM super_admins WHERE superAdmin_ID=?;";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        redirect('index.php?error=sqlerror', 'Database error', 'error');
    }
    else 
    {
        mysqli_stmt_bind_param($stmt, "s", $userid);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        if ($row = mysqli_fetch_assoc($result))
        {
            $_SESSION['sa_id'] = $row['superAdmin_ID'];
        }
        else
        {
            $sql = "SELECT * FROM admins WHERE admin_ID=?";
            $stmt = mysqli_stmt_init($conn);

            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                redirect('index.php?error=sqlerror', 'Database error', 'error');
            }
            else
            {
                mysqli_stmt_bind_param($stmt, "s", $userid);
                mysqli_stmt_execute($stmt);
                $result = mysqli_stmt_get_result($stmt);
                if ($row = mysqli_fetch_assoc($result))
                {
                    $_SESSION['admin_id'] = $row['admin_ID'];
                }
            }
        }
    }
    $template->title = 'Welcome, ' .$_SESSION['uname']. '!';
    $template->rsos = $rso->getRSOsByUniversity($_SESSION['school']);
    $template->school = $university->getUniversity($_SESSION['school'])['name'];

    if (isset($_SESSION['sa_id']))
    {
        $sid = $_SESSION['sa_id'];
        $template->events = $event->getUnapprovedEvents($sid);
    }

    echo $template;
}
else
{
    redirect('index.php', 'Must be logged in to access that page!', 'error');
}

