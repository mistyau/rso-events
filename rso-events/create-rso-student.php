<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/RSO.php'; ?>

<?php

$rso = new RSO;

if (!isset($_SESSION['userid']))
{
    redirect('index.php', 'Login to create RSOs!', 'error');
}

$userid = $_SESSION['userid'];

if (isset($_POST['rso-submit']))
{
    // Create data array
    $data = array();
    $data['name'] = $_POST['rso_name'];
    $data['description'] = $_POST['rso_description'];

    // insert into admin
    $sql = "INSERT INTO admins (admin_ID) VALUES (?)";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) 
    {
        redirect('signup.php?error=sqlerror', 'ooopsiess an error on our end occurred :(((', 'error');
    }
    mysqli_stmt_bind_param($stmt, "s", $userid);
    mysqli_stmt_execute($stmt);
    $data['admin_id'] = $userid;

    if ($rso->create($data))
    {
        redirect('dashboard.php', 'You are now the admin of your new RSO!', 'success');
    }
    else
    {
        redirect('create-rso.php', 'Something went wrong', 'error');
    }

    mysqli_stmt_close($stmt);
    mysqli_stmt_close($conn);
}

$template = new Template('templates/rso-student-create.php');

echo $template;
