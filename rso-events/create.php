<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/University.php'; ?>

<?php

$university = new University;

if (!isset($_SESSION['sa_id']))
{
    redirect('index.php', 'Login as super admin to create university profiles', 'error');
}

if (isset($_POST['university-submit']))
{
    // Create data array
    $data = array();
    $data['name'] = $_POST['uni_name'];
    $data['address'] = $_POST['uni_address'];
    $data['description'] = $_POST['uni_description'];
    $address = $data['address'];
    $prepAddr = str_replace(' ', '+', $address);
    $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' .$prepAddr.'&key=AIzaSyBCkjT_-oWgTQ17_4Atndqwzds6p2rfsAo');
    $output = json_decode($geocode);
    $latitude = $output->results[0]->geometry->location->lat;
    $longitude = $output->results[0]->geometry->location->lng;
    $data['latitude'] = $latitude;
    $data['longitude'] = $longitude;
    $data['superAdmin_id'] = $_SESSION['sa_id'];

    if ($university->create($data))
    {
        redirect('create.php', 'Your university has been created', 'success');
    }
    else
    {
        redirect('create.php', 'Something went wrong', 'error');
    }
}

$template = new Template('templates/university-create.php');

echo $template;
