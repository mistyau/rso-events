<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/University.php'; ?>

<?php
$university = new University;

if (isset($_POST['signup-submit']))
{
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['pwd'];
    $passwordRepeat = $_POST['pwd-repeat'];
    $school = $_POST['school'];

    if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat)) {
        redirect('signup.php?error=emptyfields&uid='.$username."&mail=".$email, 'Fill in all fields!', 'error');
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        redirect('signup.php', 'Invalid username and e-mail', 'error');
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        redirect('signup.php?error=invalidmail&uid='.$username.$username, 'Invalid e-mail', 'error');
    }
    else if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        redirect('signup.php?error=invaliduid&mail='.$email, 'Invalid username', 'error');
    }
    else if ($password !== $passwordRepeat) {
        redirect('signup.php?error=passwordcheck&uid='.$username.'&mail='.$email, 'Passwords do not match', 'error');
    }
    else {
        $sql = "SELECT username FROM users WHERE username=?";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            redirect('signup.php?error=sqlerror', 'Database error', 'error');
        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if ($resultCheck > 0) {
                redirect('signup.php?error=usertaken&mail='.$email, 'Username taken', 'error');
            }
            else {
                $sql = "INSERT INTO users (username, email, pwd, school) VALUES (?, ?, ?, ?)";
                $stmt = mysqli_stmt_init($conn);
                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    redirect('signup.php?error=sqlerror', 'ooopsiess an error on our end occurred :(((', 'error');
                }
                else {
                    $hashedpwd = password_hash($password, PASSWORD_DEFAULT);
                    mysqli_stmt_bind_param($stmt, "ssss", $username, $email, $hashedpwd, $school);
                    mysqli_stmt_execute($stmt);
                    $userid = mysqli_insert_id($conn);

                    /*
                    if ($userType == 1)
                    {
                        // do nothing
                    }
                    else if ($userType == 2)
                    {
                        // insert into admin
                        $sql = "INSERT INTO admins (userid, university) VALUES (?, ?)";
                        $stmt = mysqli_stmt_init($conn);
                        if (!mysqli_stmt_prepare($stmt, $sql)) 
                        {
                            redirect('signup.php?error=sqlerror', 'ooopsiess an error on our end occurred :(((', 'error');
                        }
                        mysqli_stmt_bind_param($stmt, "ss", $userid, $school);
                        mysqli_stmt_execute($stmt);
                    }
                    else if ($userType == 3)
                    {
                        // insert into super admins
                        $sql = "INSERT INTO super_admins (userid) VALUES (?)";
                        $stmt = mysqli_stmt_init($conn);
                        if (!mysqli_stmt_prepare($stmt, $sql)) 
                        {
                            redirect('signup.php?error=sqlerror', 'ooopsiess an error on our end occurred :(((', 'error');
                        }
                        mysqli_stmt_bind_param($stmt, "s", $userid);
                        mysqli_stmt_execute($stmt);
                    }
                    else
                    {
                        redirect('signup.php?signup=error', 'Something went wrong', error);
                    }
                    */
                    redirect('signup.php?signup=success', 'Signup successful!', 'success');
                }
            }
        }
    }

    mysqli_stmt_close($stmt);
    mysqli_stmt_close($conn);
}

$template = new Template('templates/create-user.php');
$template->universities = $university->getAllUniversities();

echo $template;