<?php require __DIR__ . '/vendor/autoload.php'; ?>

<?php include_once 'config/init.php'; ?>

<?php require_once 'lib/Event.php'; ?>

<?php
$event = new Event;

if (!isset($_SESSION['userid']))
{
    redirect('index.php', 'Sign in to access this page', 'error');
}
else
{
    $userid = $_SESSION['userid'];
}

if (isset($_POST['comment-submit']))
{
    $data = array();
    $data['text'] = $_POST['comment'];
    if (isset($_POST['rate']))
    {
        $rating = $_POST['rate'];
    }
    else
    {
        $rating = NULL;
    }
    $data['rating'] = $rating;
    $time = time();
    $data['timestamp'] = date('Y-m-d H:i:s', $time);
    $data['eventid'] = isset($_GET['id']) ? $_GET['id'] : null;

    if ($event->comment($userid, $data))
    {
        redirect('event.php?id='.$data['eventid'], 'Comment posted', 'success');
    }
    else
    {
        redirect('event.php?id='.$data['eventid'], 'Something went wrong', 'error');
    }
}

if (isset($_POST['del_id']))
{
    $del_id = $_POST['del_id'];
    if ($event->deleteComment($userid, $del_id))
    {
        redirect('event.php?id='.$del_id, 'Successfully deleted comment', 'success');
    }
    else
    {
        redirect('event.php?id='.$del_id, 'Something went wrong', 'error');
    }
}

if (isset($_POST['edit-submit']))
{
    $data = array();
    $data['eid'] = isset($_GET['id']) ? $_GET['id'] : null;
    $data['text'] = $_POST['new_comment'];
    $timestamp = time();
    $data['time'] = date('Y-m-d H:i:s', $timestamp);

    if ($event->editComment($userid, $data))
    {
        redirect('event.php?id='.$data['eid'], 'Successfully edited comment', 'success');
    }
    else
    {
        redirect('event.php?id='.$data['eid'], 'Something went wrong', 'error');
    }
}

$template = new Template('templates/event-single.php');

$event_id = isset($_GET['id']) ? $_GET['id'] : null;

$template->event = $event->getEvent($event_id);
$template->comments = $event->getAllComments($event_id);

echo $template;