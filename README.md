# RSO Events

This project is a web application to organize RSOs (Registered Student Organizations) and college events. Users can sign in as a student, admin, or super admin. Students can view events, comment on events, and request for RSOs to be made. Admins own RSOs and can host events for their respective RSOs, as well as request approval from a super admin for a public/private event to be made. Super admins can approve events and create university profiles.

Sign-in:
![alt text](img/signin.png "Sign-in Page")

Create an account:
![alt text1](img/createaccount.png "Create Account Page")

Dashboard:
![alt text2](img/dashboard.png "Dashboard Page")

Create an event:
![alt text2](img/create_event.png "Create Event Page")
![alt text2](img/create_event2.png "Create Event Page Cont.")

Comments:
![alt text](img/comments.png "Comments Page")